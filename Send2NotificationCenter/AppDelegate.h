//
//  AppDelegate.h
//  Send2NotificationCenter
//
//  Created by Frantisek Erben on 22.02.18.
//  Copyright © 2018 Frantisek Erben. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

