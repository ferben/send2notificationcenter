//
//  AppDelegate.m
//  Send2NotificationCenter
//
//  Created by Frantisek Erben on 22.02.18.
//  Copyright © 2018 Frantisek Erben. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
   // Register myself as delegate
   [[NSUserNotificationCenter defaultUserNotificationCenter] setDelegate: (id)self];
}

- (BOOL)userNotificationCenter:(NSUserNotificationCenter *)center shouldPresentNotification:(NSUserNotification *)notification{
   return YES;
}

- (IBAction)SendNotification:(id *)sender {
   NSUserNotification *notification = [[NSUserNotification alloc] init];
   NSUserNotificationCenter *nc = [NSUserNotificationCenter defaultUserNotificationCenter];
   [notification setHasActionButton:NO];
   [notification setTitle: [NSBundle mainBundle].localizedInfoDictionary[@"CFBundleDisplayName"]]; // Application name
   [notification setSubtitle:@"Beze jména.wfd"];
   [notification setInformativeText: @"Process finished successfully!"];
   [notification setSoundName: NSUserNotificationDefaultSoundName];
   [nc deliverNotification:notification]; // After 15 minut notification banner disapear, but stay in Notification Center: https://developer.apple.com/documentation/foundation/nsusernotificationcenter?language=objc
   /*
   // Scheduled notification show delete after timeout (does not stay in Notification Center anymore)
   NSTimeInterval delayBeforeDelivering = 0.0;
   NSTimeInterval delayBeforeDismissing = 3.0;
   [notification setDeliveryDate:[NSDate dateWithTimeIntervalSinceNow:delayBeforeDelivering]];
   [nc scheduleNotification:notification];
   [nc performSelector:@selector(removeDeliveredNotification:)
            withObject:(notification)
            afterDelay:(delayBeforeDelivering + delayBeforeDismissing)];
    */
}  

- (void)applicationWillTerminate:(NSNotification *)aNotification {
   // Cleanup all notifications from Notification Center 
   //[[NSUserNotificationCenter defaultUserNotificationCenter] removeAllDeliveredNotifications];

}


@end
