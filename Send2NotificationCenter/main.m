//
//  main.m
//  Send2NotificationCenter
//
//  Created by Frantisek Erben on 22.02.18.
//  Copyright © 2018 Frantisek Erben. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
   return NSApplicationMain(argc, argv);
}
